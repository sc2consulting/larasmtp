<div style="border: black 1px solid">
    <form method="post" action="/transporter/add">
        @csrf
        <div>
            <label for="host">host</label>
            <input type="text" id="host" name="host" required>
        </div>
        <div>
            <label for="port">port</label>
            <input type="text" id="port" name="port" required>
        </div>
        <div>
            <label for="username">username</label>
            <input type="text" id="username" name="username">
        </div>
        <div>
            <label for="password">password</label>
            <input type="text" id="password" name="password">
        </div>
        <div>
            <label for="method">method</label>
            <input type="text" id="method" name="method" required>
        </div>
        <button type="submit">send</button>
    </form>
</div>
