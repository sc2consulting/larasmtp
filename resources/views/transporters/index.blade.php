@extends('layouts.base')

@section('content')
    <div>
        <div>
            <button id="add_transporter">add</button>
        </div>
        <div>
            <table>
                <tr>
                    <th>id</th>
                    <th>host</th>
                    <th>port</th>
                    <th>username</th>
                    <th>password</th>
                    <th>method</th>
                    <th>actions</th>
                </tr>
                @foreach($transporters as $transporter)
                    <tr>
                        <td>{{ $transporter->id }}</td>
                        <td>{{ $transporter->host }}</td>
                        <td>{{ $transporter->port }}</td>
                        <td>{{ $transporter->username }}</td>
                        <td>{{ $transporter->password }}</td>
                        <td>{{ $transporter->method }}</td>
                        <td>
                            <button id="delete" onclick="delete_transporter({{ $transporter->id }})">remove</button>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div>
        @include('transporters.add')
    </div>

@endsection

<script>
    function add_transporter() {

    }

    function delete_transporter(transporter_id) {

    }
</script>