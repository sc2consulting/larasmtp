<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Test Lists
        </h2>
    </x-slot>

    <div>
        <table class="table-auto border border-gray-600">
            <thead>
            <tr class="border border-gray-600">
                <th>ID</th>
                <th>Name</th>
                <th>Emails</th>
                <th>Last used</th>
                <th>Option</th>
            </tr>
            </thead>

            <tbody>
            @foreach($email_lists as $list)
                <tr class="border border-gray-600">
                    <td>{{ $list->id }}</td>
                    <td>{{ $list->name }}</td>
                    <td>
                        @foreach($list->emails as $email)
                            <ul>{{ $email->email }}</ul>
                        @endforeach
                    </td>
                    <td>{{ $list->last_used }}</td>
                    <td><button onclick="delete_list({{ $list->id }})"><i class="fa fa-close"></i></button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <form>
            <div>
                <p><label for="name">name</label></p>
                <p><input id="name" type="text"></p>
            </div>
            <div>
                <p><label for="emails">emails</label> (1 email par ligne)</p>
                <p><textarea id="emails"></textarea></p>
            </div>
            <button onclick="add_list()">submit</button>
        </form>
    </div>

    <script>
        function add_list() {
            let data = new FormData()
            data.append('name', document.getElementById('name').value)
            data.append('emails', document.getElementById('emails').value)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    alert(this.status)
                    if (this.status === 200)
                        location.reload()
                    else
                        console.log(this.responseText)
                }
            }
            request.open("POST", "/test_lists/add", true)
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data);
        }

        function delete_list(id) {
            let data = new FormData()
            data.append('test_list_id', id)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    alert(this.status)
                    if (this.status === 200)
                        location.reload()
                    else
                        console.log(this.responseText)
                }
            }
            request.open("POST", "/test_lists/remove", true)
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data);
        }
    </script>
</x-app-layout>
