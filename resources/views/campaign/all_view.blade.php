<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Campaigns
        </h2>
    </x-slot>

    <div class="grid grid-cols-4">
        <div class="col-span-3">
            @foreach($campaigns as $campaign)
                <p><button onclick="del_campaign({{ $campaign->id }})">[x]</button> - <a href="/campaign/get/{{ $campaign->id }}">{{ $campaign->name }}</a></p>
            @endforeach
        </div>
        <div>
            @include('campaign.component.add')
        </div>
    </div>

    <script>
        function del_campaign(campaign_id)
        {
            let data = new FormData()
            data.append('campaign_id', campaign_id)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function ()  {
                if (this.readyState === 4) {
                    let res = JSON.parse(this.response)
                    let message = this.status + ' : ' + res['message']
                    alert(message)
                    if (this.status === 200)
                        location.reload()
                }
            }
            request.open("POST", "/campaign/delete", true);
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data);
        }
    </script>
</x-app-layout>
