<p>email(s) in list : {{ $campaign->emails->count() }}</p>

<div>
    <ul>
        @foreach($emails as $email)
            <li>{{ $email->email }} - <button onclick="remove_from_campaign({{ $campaign->id }}, {{ $email->id }})">[x]</button></li>
        @endforeach
    </ul>
</div>

<script>
    function remove_from_campaign(campaign_id, email_id) {
        let data = new FormData()
        data.append('campaign_id', campaign_id)
        data.append('email_id', email_id)

        let request = new XMLHttpRequest()
        request.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200)
                    location.reload()
                else
                    alert(this.status)
            }
        }
        request.open("POST", "/campaign/email/delete", true)
        request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
        request.send(data)

    }
</script>
