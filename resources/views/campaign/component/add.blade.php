<div>
    <div>
        <label for="name">Campaign Name</label>
        <input id="name" name="name" type="text" required>
    </div>

    <div>
        <label for="subject">Subject</label>
        <input id="subject" name="subject" type="text" required>
    </div>

    <div>
        <label for="sender_name">Sender Name</label>
        <input id="sender_name" name="sender_name" type="text" required>
    </div>

    <div>
        <label for="mail_content">body content/html</label>
        <input id="mail_content" name="mail_content" type="file" required>
    </div>

    <div>
        <label for="transporter_id">transporter</label>
        <select id="transporter_id" name="transporter_id" required>
            @foreach($transporters as $transporter)
                <option value={{ $transporter->id }}>{{ $transporter->name }}</option>
            @endforeach
        </select>
    </div>

    <div>
        <button onclick="send_create()">Send</button>
    </div>
</div>
<script>
    function send_create() {
        const file = document.getElementById('mail_content').files[0]

        if (file === undefined) {
            document.getElementById('mail_content').style.color = 'red'
            return
        }

        let data = new FormData()
        data.append('name', document.getElementById('name').value)
        data.append('subject', document.getElementById('subject').value)
        data.append('sender_name', document.getElementById('sender_name').value)
        data.append('transporter_id', document.getElementById('transporter_id').value)
        data.append('mail_content', file)

        let request = new XMLHttpRequest()
        request.onreadystatechange = function ()  {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    alert("campaign has been added.")
                    location.reload()
                }
                else {
                    alert(this.status)
                    console.log(this.responseText)
                }
            }
        }
        request.open("POST", "/campaign/store", true)
        request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
        request.send(data);
    }
</script>
