<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $campaign->name }}
        </h2>
    </x-slot>

    <div class="grid grid-cols-3">
        <div class="col-span-2">
            <div>
                <ul>
                    <li>subject : {{ $campaign->subject }}</li>
                    <li>sender : {{ $campaign->sender_name }}</li>
                </ul>
            </div>

            <div>
                <input type="hidden" name="campaign_id" value="{{ $campaign->id }}">
                <label for="mail_list_id">test list :</label>
                <select id="mail_list_id" name="mail_list_id" required>
                    @foreach($test_lists as $list)
                        <option value="{{ $list->id }}">{{ $list->name }}</option>
                    @endforeach
                </select>
                <button class="border-2" onclick="exec_test()">send test email</button>
            </div>

            <div>
                <div>
                    <input id="campaign_id" name="campaign_id" type="hidden" value="{{ $campaign->id }}">
                    <label for="email_file">add email :</label>
                    <input id="email_file" name="email_file" type="file">
                    <button onclick="send_email_list()">add_email_file</button>
                </div>
            </div>

            <div>
                <button onclick="exec_campaign()">send</button>
            </div>

        </div>
        <div class="">
            @include('campaign.component.editOne')
        </div>
        <div class="col-span-2">
            @include('campaign.component.email_list', ['campaign' => $campaign, 'emails' => $campaign->emails])
        </div>
    </div>


    <script>
        function send_email_list() {
            const file = document.getElementById('email_file').files[0]

            if (file === undefined) {
                document.getElementById('email_file').style.color = 'red'
                return
            }

            let data = new FormData()
            data.append('campaign_id', document.getElementById('campaign_id').value)
            data.append('email_file', file)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    let res = JSON.parse(this.response)
                    let message = this.status + ' : ' + res['message']
                    alert(message)
                    if (this.status === 200)
                        location.reload()
                }
            }
            request.open("POST", "/campaign/add_emails", true)
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data)
        }

        function exec_campaign() {
            let data = new FormData()
            data.append('campaign_id', {{ $campaign->id }})

            let request = new XMLHttpRequest()
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        let response = JSON.parse(this.responseText)
                        alert("result = " +
                            response['result']['success'] + " sent & " +
                            response['result']['error'] + " error"
                        )
                        location.reload()
                    }
                    else
                        alert(this.status + " : " + this.responseText)
                }
            }
            request.open("POST", "/campaign/exec", true)
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data)
        }

        function exec_test() {
            let data = new FormData()
            data.append('campaign_id', {{ $campaign->id }})
            data.append('mail_list_id', document.getElementById("mail_list_id").value)

            let request = new XMLHttpRequest()
            request.onreadystatechange = function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        let response = JSON.parse(this.responseText)
                        alert("result = " +
                            response['result']['success'] + " sent & " +
                            response['result']['error'] + " error"
                        )
                        location.reload()
                    }
                    else
                        alert(this.status + " : " + this.responseText)
                }
            }
            request.open("POST", "/campaign/exec_test", true)
            request.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}")
            request.send(data)

        }
    </script>
</x-app-layout>
