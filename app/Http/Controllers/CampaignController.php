<?php

namespace App\Http\Controllers;

use App\Helpers\MailHub;
use App\Helpers\ReqTools;
use App\Models\Campaign;
use App\Models\Campaign_Email;
use App\Models\Email;
use App\Models\File;
use App\Models\TestEmailList;
use App\Models\Transporter;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	// Campaign Management
	public function getAllView()
	{
		return view('campaign.all_view')
			->with('campaigns', Campaign::all())
			->with('transporters', Transporter::all());
	}

	public function getOneView($id)
	{
		if (intval($id) && $campaign = Campaign::whereId($id)->first())
			return view('campaign.single_view')
				->with('campaign', $campaign)
				->with('transporters', Transporter::all())
				->with('test_lists', TestEmailList::orderByDesc('last_used')->get());

		return view('error')
			->with('message', 'campaign not found');
	}

	public function addOne(Request $request)
	{
		$required = [
			'name',
			'subject',
			'sender_name',
			'transporter_id'
		];
		if (!ReqTools::notNullArgs($request, $required))
			return response(['message' => 'missing argument'], 400);
		if (!$request->hasFile('mail_content'))
			return response(['message' => 'no mail content'], 400);

		$file = File::store($request->file('mail_content'), '/campaign/html');

		$campaign = new Campaign();
		$campaign->name = $request->get('name');
		$campaign->subject = $request->get('subject');
		$campaign->sender_name = $request->get('sender_name');
		$campaign->body = $file->id;
		$campaign->transporter_id = $request->get('transporter_id');
		$campaign->save();
		return response(['message' => 'campaign added']);
	}

	public function editOne(Request $request)
	{
		// todo process campaign edit
	}

	public function deleteOne(Request $request)
	{
		if (!$request->has('campaign_id'))
			return response(['message' => 'Missing campaign_id'], 400);
		if (!$campaign = Campaign::whereId($request->get('campaign_id'))->first())
			return response(['message' => 'Campaign does not exists.'], 400);

		if ($campaign->send_count > 0)
			return response(['message' => 'Cannot delete used campaign.'], 400);

		Campaign_Email::whereCampaignId($campaign->id)->delete();
		$campaign->delete();
		return response(['message' => 'Campaign removed']);
	}

	public function addEmailList(Request $request)
	{
		if (!$request->has('campaign_id'))
			return response(['message' => 'missing campaign_id'], 400);
		if (!$request->has('email_file'))
			return response(['message' => 'missing email_file'], 400);
		if (!$campaign = Campaign::whereId($request->get('campaign_id'))->first())
			return response(['message' => 'campaign does not exists'], 400);

		$file = File::store($request->file('email_file'), '/email_file');
		$email_id_list = Email::store_file($file);
		if ($email_id_list === false)
			return response(['message' => "invalid email provided in file"], 400);
		$count = 0;
		$duplicate = 0;
		foreach ($email_id_list as $email_id) {
			if ($campaign->emails->contains($email_id))
				$duplicate += 1;
			else {
				$relation = new Campaign_Email();
				$relation->campaign_id = $campaign->id;
				$relation->email_id = $email_id;
				$relation->save();
				++$count;
			}
		}
		return response(
			['message' => 'Added emails to campaign',
				'data' => [
					'success' => $count,
					'duplicate' => $duplicate
				]
			]);
	}

	public function removeEmail(Request $request)
	{
		if (!$request->has(['campaign_id', 'email_id']))
			return response(['message' => 'missing argument'], 400);
		if (!$campaign = Campaign::whereId($request->get('campaign_id'))->first())
			return response(['message' => 'campaign does not exists'], 400);
		if (!$campaign->emails()->where('id', $request->get('email_id')))
			return response(['message' => 'email not in list'], 400);

		$campaign->emails()->detach($request->get('email_id'));
		return response(['message' => 'email removed']);
	}

	public function removeEmails(Request $request)
	{
		// todo remove list of email from campaign
	}

	// campaign functions
	public function exec_campaign(Request $request)
	{
		if (!$request->has('campaign_id'))
			return ['status' => 400, 'message' => 'no campaign provided'];
		if (!$campaign = Campaign::whereId($request->get('campaign_id'))->first())
			return ['status' => 400, 'message' => 'campaign does not exists'];
		$results = MailHub::runMailer($campaign);
		return response(['message' => "emails sent", 'data' => $results]);
	}

	public function exec_test(Request $request)
	{
		if (!$request->has('campaign_id'))
			return response(['message' => 'missing campaign_id'], 400);
		if (!$request->has('mail_list_id'))
			return response(['message' => 'missing mail_list_id'], 400);
		$campaign = Campaign::whereId($request->get('campaign_id'))->first();
		$mail_list = TestEmailList::whereId($request->get('mail_list_id'))->first();

		$result = MailHub::runMailer($campaign, $mail_list->get_emails());
		$mail_list->last_used = date("Y-m-d H:i:s");
		$mail_list->save();

		return response([
			'message' => 'ok',
			'data' => [$campaign->name, $mail_list->name],
			'result' => $result
		]);
	}
}
