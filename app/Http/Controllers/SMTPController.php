<?php

namespace App\Http\Controllers;

use App\Helpers\ApiCall;
use App\Helpers\MailFormatter;
use App\Models\Campaign;
use App\Models\Email;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Swift_SmtpTransport;

class SMTPController extends Controller
{
	/**
	 * @var Campaign
	 */
	private $campaign;
	private $emails = [];
	private $body;
	private $results = [];

	public function __construct(Campaign $campaign)
	{
		$this->campaign = $campaign;
		$file = File::whereId($campaign->body)->first();
		$this->body = file_get_contents($file->path);
	}

	public function loadEmails()
	{
		$emails = $this->campaign->emails()->get();
		foreach ($emails as $email) {
			array_push($this->emails, $email);
		}
		return count($this->emails);
	}

	/**
	 * @param array $emails
	 */
	public function setEmails(array $emails): void
	{
		$this->emails = $emails;
	}

	public function getEmails()
	{
		return $this->emails;
	}


	// sending
	public function send()
	{

		$mailFormatter = new MailFormatter($this->body);
		$this->campaign->update_send_count();

//		/** @var Email $email */
//		foreach ($this->emails as $email) {
//			$body = $mailFormatter->getCampaignBody($email, $this->campaign);
//
//			$message = new \Swift_Message($this->campaign->subject);
//			$message->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME')); // todo: replace with Model::Sender
//			$message->setTo($email->email);
//			$message->setContentType('text/html');
//			$message->setBody($body);
//
//			$result = $mailer->send($message);
//			array_push($this->results, $result);
//		}
		if ($this->campaign->transporter->method == 'smtp')
			return $this->send_smtp($mailFormatter);
		if ($this->campaign->transporter->method == 'ovh-router')
			return $this->send_ovh_router($mailFormatter);
		return $this->results;
	}

	private function send_smtp(MailFormatter $mailFormatter): array
	{
		$transporter = $this->campaign->getSwiftTransporterObject();
		$mailer = new \Swift_Mailer($transporter);

		/** @var Email $email */
		foreach ($this->emails as $email) {
			$body = $mailFormatter->getCampaignBody($email, $this->campaign);

			$message = new \Swift_Message($this->campaign->subject);
			$message->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
			$message->setTo($email->email);
			$message->setContentType('text/html');
			$message->setBody($body);

			$result = $mailer->send($message);
			array_push($this->results, $result);
		}
		return $this->results;
	}

	private function send_ovh_router(MailFormatter $mailFormatter): array
	{
		$result = [];
		$url = $this->campaign->transporter->host;

		foreach ($this->emails as $email) {
			$body = $mailFormatter->getCampaignBody($email, $this->campaign);

			// set sender from DB
			$data = [
				'sender' => $this->campaign->sender_name,
				'recipient_email' => $email->email,
				'recipient_name' => '',
				'subject' => $this->campaign->subject,
				'body' => $body
			];

			$response = ApiCall::send('POST', $url, false, $data);
			Log::info($response);
		}
		return $result;
	}

	private function generate_alt_body(string $body)
	{
		return "just a temporary plain text";
	}
}
