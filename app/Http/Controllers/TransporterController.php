<?php

namespace App\Http\Controllers;

use App\Models\Transporter;
use Illuminate\Http\Request;

class TransporterController extends Controller
{
	public function getTransporterView() {
		$transporters = Transporter::all();
		return view('transporters.index')
			->with('transporters', $transporters);
	}

	public function add(Request $request)
	{
		$required = ['host', 'port', 'username', 'password', 'method'];
		if (!$request->has($required))
			return ["status" => 400, "missing parameter"];

		$new_transporter = new Transporter();
		$new_transporter->host = $request->get('host');
		$new_transporter->port = $request->get('port');
		$new_transporter->username = $request->get('username') ?: "";
		$new_transporter->password = $request->get('password') ?: "";
		$new_transporter->method = $request->get('method');
		$new_transporter->save();
		return response(['message' => 'ok']);
	}

	public function remove(Request $request)
	{
		if (!$request->has('transporter_id'))
			return ['status' => 400, 'message' => 'missing transporter_id'];
		if (!$transporter = Transporter::whereId($request['transporter_id']))
			return ['status' => 400, 'message' => 'transporter unknown'];
		$transporter->delete();
		return response(['message' => 'transporter deleted.']);
	}

	public function edit(Request $request)
	{
		if (!$request->has('transporter_id'))
			return response(['message' => 'Missing transporter_id.'], 400);
		$transporter = Transporter::whereId($request->get('transporter_id'));
		$info = $request->all();
		$transporter->update($info);
		$transporter->save();
		return response(['message' => 'Transporter updated.']);
	}
}
