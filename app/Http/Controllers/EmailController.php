<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Fai;
use App\Models\File;
use Illuminate\Http\Request;

class EmailController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getOneView(Request $request)
	{

	}

	public function importFile(Request $request)
	{

	}

	public function importUnsubscribe(Request $request)
	{

	}

	public function get_email_detail(Request $req)
    {
    	if ($req->has('email')) {
    		return Email::whereEmail($req->get('email'))->first();
	    }
    	return response(['message' => 'no mail given'], 400);
    }

    public function add_from_file(Request $req)
    {
    	if (!$req->hasFile('email_file')) {
		    return ["status" => 400, "message" => "no file 'email_file' found"];
	    }
    	$file = File::store($req->file('email_file'), '/email_file');
    	// Todo
	    $fh = fopen($file->path, 'r');
	    $result = [
		    'inserted' => 0,
	        'invalid' => 0,
	        'duplicate' => 0
	    ];
	    while ($line = fgets($fh)) {
	    	if (filter_var($line, FILTER_VALIDATE_EMAIL))
	    		$result['invalid']++;
		    else if (Email::whereEmail($line)->first())
		    	$result['duplicate']++;
		    else {
		    	$new_email = new Email();
		    	$new_email->email = $line;
			    $new_email->fai_id = Fai::get_fai_id($line);
			    $new_email->save();
			    $result['inserted']++;
		    }
	    }
	    return response([
		    "message" => "emails added",
		    'data' => $result
	    ]);
    }

    public function upload_unsub(Request $request)
    {
    	if (!$request->hasFile('email_file')) {
    		return response(["message" => 'no file found.'], 400);
	    }

    	$file = File::store($request->file('email_list'), '/unsubscribe');
    	$fh = fopen($file->path, 'r');
	    $result = [
	    	'unsubscribed' => 0,
		    'invalid' => 0
	    ];
	    while ($line = fgets($fh)) {
		    if (filter_var($line, FILTER_VALIDATE_EMAIL))
			    $result['invalid']++;
		    else {
		    	$email = Email::whereEmail($line);
		    	$email->unsubscribe = true;
		    	$email->save();
		    	$result['unsubscribed']++;
		    }
	    }
	    return response(['message' => 'unsubscribed', 'data' => $result]);
    }
}
