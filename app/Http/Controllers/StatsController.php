<?php

namespace App\Http\Controllers;

use App\Helpers\SCrypt;
use App\Models\Campaign;
use App\Models\Email;
use App\Models\EmailCampaignClcik;
use App\Models\EmailCampaignOpen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class StatsController extends Controller
{
	public function unsubscribe($data) // todo move function to EmailController
	{
		if (!$email_hash = SCrypt::decrypt($data))
			return view('unsubscribe.invalid');
		if (!$email = Email::whereSha256($email_hash)->first()) {
			return view('unsubscribe.invalid');
		}
		if ($email->unsubscribe) {
			return view('unsubscribe.invalid');
		}
		$email->unsubscribe = true;
		$email->save();
		return view('unsubscribe.success');
	}

	public function open($data)
	{
		Log::info('raw data = '. $data);
		$data = explode('.', $data);
		if (count($data) !== 4)
			return;
		$email_sha = SCrypt::decrypt($data[0] . '.' . $data[1]);
		if (!$email = Email::whereSha256($email_sha)->first())
			return;
		$campaign_id = intval($data[2]);
		$send_nb = intval($data[3]);

		// verify unique
		if (EmailCampaignOpen::whereCampaignId($campaign_id)
			->where('email_id', $email->id)
			->where('campaign_send_number', $send_nb)
			->first())
			return;

		// update campaign and email stats
		if (!$campaign = Campaign::whereId($campaign_id)->first())
			return;
		$c_stats = $campaign->stats;
		$c_stats->open += 1;
		$c_stats->save();
		$email->open += 1;
		$email->save();

		// create relation
		$open_rel = new EmailCampaignOpen();
		$open_rel->campaign_id = $campaign->id;
		$open_rel->email_id = $email->id;
		$open_rel->campaign_send_number = $send_nb;
		$open_rel->save();
	}

	public function click($data)
	{
		$data = explode('.', $data);
		if (count($data) !== 5)
			return view('error')->with('message', 'invalid link');
		$redirect_url = base64_decode(str_replace('-', '/', $data[4]));
		$email_sha = SCrypt::decrypt($data[0] . '.' . $data[1]);
		if (!$email = Email::whereSha256($email_sha)->first())
			$redirect_url = base64_decode(str_replace('-', '/', $data[4]));
		$campaign_id = intval($data[2]);
		$send_nb = intval($data[3]);

		// verify unique
		if (EmailCampaignClcik::whereCampaignId($campaign_id)
			->where('email_id', $email->id)
			->where('campaign_send_number', $send_nb)
			->first())
			return Redirect::to($redirect_url);

		// update campaign and email stats
		if (!$campaign = Campaign::whereId($campaign_id)->first())
			return Redirect::to($redirect_url);
		$c_stats = $campaign->stats;
		$c_stats->click += 1;
		$c_stats->save();
		$email->click += 1;
		$email->save();

		// create relation
		$open_rel = new EmailCampaignClcik();
		$open_rel->campaign_id = $campaign->id;
		$open_rel->email_id = $email->id;
		$open_rel->campaign_send_number = $send_nb;
		$open_rel->save();

		return Redirect::to($redirect_url);
	}
}
