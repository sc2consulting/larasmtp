<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\TestEmailList;
use App\Models\TestListEmailRelation;
use Illuminate\Http\Request;

class TestEmailListController extends Controller
{
	public function getView()
	{
		return view('test_mail_list.index')
			->with(['email_lists' => TestEmailList::all()]);
	}

    public function add_list(Request $request)
    {
	    $emails = explode("\n", $request->get('emails'));
	    $invalid_emails = [];
	    foreach ($emails as $email)
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		    	$invalid_emails []= $email;
	    if (count($invalid_emails))
		    return response(['message' => 'invalid email(s) given', 'data' => $invalid_emails]);

	    $email_ids = [];
	    foreach ($emails as $test_email) {
		    $email = Email::whereEmail($test_email)->first();
		    if (!$email) {
			    $email = new Email($test_email);
			    $email->save();
		    }
		    $email_ids []= $email->id;
	    }

	    $test_list = new TestEmailList();
	    $test_list->name = $request->get('name');
	    $test_list->last_used = date("Y-m-d H:i:s");
	    $test_list->save();

	    foreach ($email_ids as $email_id) {
		    $relation = new TestListEmailRelation();
		    $relation->email_id = $email_id;
		    $relation->test_email_list_id = $test_list->id;
		    $relation->save();
	    }

	    return response(['message' => 'ok']);
    }

    public function remove_list(Request $request)
    {
	    TestEmailList::whereId($request->get('test_list_id'))->first()->delete();
	    return response(['message' => 'ok']);
    }
}
