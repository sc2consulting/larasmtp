<?php


namespace App\Helpers;


use App\Helpers\Mailers\OvhRouter;
use App\Models\Campaign;
use App\Models\File;
use Illuminate\Support\Facades\Log;

abstract class MailHub
{
	/**
	 * @var Campaign
	 */
	protected $campaign;
	/**
	 * @var array
	 */
	protected $emails = [];
	protected $sender;
	protected $body;
	protected $results = [];

	public function __construct(Campaign $campaign, array $emails = null)
	{
		$this->campaign = $campaign;
		$this->sender = $campaign->sender_name;
		if ($emails)
			$this->emails = $emails;
		else
			foreach ($this->campaign->emails as $email)
				$this->emails []= $email;
		$file = File::whereId($campaign->body)->first();
		$this->body = file_get_contents($file->path);
	}

	abstract public function send_mail() : array;

	abstract public static function add_to_DB() : bool;

	// default route for sending, will redirect to the right sender
	static public function runMailer(Campaign $campaign, array $emails = null)
	{
		$campaign->send_count += 1;
		$campaign->save();
		$result = [];

		switch ($campaign->transporter_id) {
			case 1: // OVH Router
				$transporter = new OvhRouter($campaign, $emails);
				$result = $transporter->send_mail();
				break;
			default :
				break;
		}
		return $result;
	}
}