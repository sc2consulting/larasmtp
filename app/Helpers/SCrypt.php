<?php


namespace App\Helpers;


class SCrypt
{
	public static function encrypt(string $data, string $key)
	{
		$salt = env('SCRYPT_SALT');
		$salt = str_pad($salt, strlen($data), $salt);

		$_key = str_pad($key, strlen($data), $key);

		$crypt_key = $_key ^ $salt;
		$crypt = $data ^ $crypt_key;
		$crypt = base64_encode($crypt);
		return $key . "." . str_replace('/', '-', $crypt);
	}

	public static function decrypt(string $data)
	{
		$tmp = explode('.',$data);
		if (count($tmp) != 2)
			return null;
		$salt = env('SCRYPT_SALT');
		$key = str_pad($tmp[0], strlen($data), $tmp[0]);
		$crypt = str_replace('-', '/', $tmp[1]);
		$crypt = base64_decode($crypt);
		$crypt_key = $key ^ str_pad($salt, strlen($crypt), $salt);
		return $crypt ^ $crypt_key;
	}
}