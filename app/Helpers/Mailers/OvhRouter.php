<?php


namespace App\Helpers\Mailers;


use App\Helpers\ApiCall;
use App\Helpers\MailerInterface;
use App\Helpers\MailFormatter;
use App\Helpers\MailHub;
use App\Models\Campaign;
use App\Models\Email;
use App\Models\Transporter;
use Illuminate\Support\Facades\Log;

class OvhRouter extends MailHub
{
	static private $name = 'ovh_router';

	public function __construct(Campaign $campaign, $emails = null)
	{
		parent::__construct($campaign, $emails);
	}

	public function send_mail() : array
	{
		$mail_formatter = new MailFormatter($this->body);
		$result = ['success' => 0, 'error' => 0];
		$url = env('OVH_ROUTER_URL');

		foreach ($this->emails as $email) {
			$body = $mail_formatter->getCampaignBody($email, $this->campaign);

			$data = [
				'sender_name' => $this->campaign->sender_name,
				'recipient_email' => $email->email,
				'recipient_name' => '',
				'subject' => $this->campaign->subject,
				'body' => $body
			];

			$response = ApiCall::send('POST', $url, false, $data);
			if (($response->status) && $response->status === 200)
				$result['success'] += 1;
			else {
				$result['error'] += 1;
			}
		}
		return $result;
	}

	public static function add_to_DB(): bool
	{
		if (Transporter::whereName(self::$name)->first())
			return false;
		$transporter = new Transporter();
		$transporter->name = self::$name;
		$transporter->save();
		return true;
	}
}