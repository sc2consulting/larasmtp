<?php


namespace App\Helpers;


class SRegex
{
	public static function add_a_href_tracker(string $subject, $replacement_header)
	{
		$regex = '/<a[^>]*href="\K[^"]*/';
		$result = preg_replace_callback($regex, function ($matches) use ($replacement_header) {
//			$replacements = array();
//			var_dump($matches);
//			foreach ($matches as $match) {
//				$encoded_url = base64_encode($match);
//				$replacement = $replacement_header . str_replace('/', '-', $encoded_url);
//				array_push($replacements , $replacement);
//			}
//			var_dump($replacements);
//			return $replacements;

			$encoded_url = base64_encode($matches[0]);
			$replacement = $replacement_header . str_replace('/', '-', $encoded_url);
			return $replacement;
		}, $subject);
		return $result;
	}
}