<?php


namespace App\Helpers;


use App\Models\Campaign;
use App\Models\Email;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class MailFormatter
{
	private $body;

	public function __construct(string $body)
	{
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getCampaignBody(Email $email, Campaign $campaign): string
	{
		$body = $this->body;
		$body = $this->set_click_tracker($body, $email, $campaign);
		$body = $this->set_open_tracker($body, $email, $campaign);
		$body = $this->set_unsub_link($body, $email);
		return $body;
	}

	private function set_click_tracker(string $body, Email $email, Campaign $campaign)
	{
		$encrypted_email = SCrypt::encrypt($email->sha256, $email->crypt_key);
		$campaign_id = $campaign->id;
		$c_send_count = $campaign->send_count;
		$tracker_url = URL::to("/click/$encrypted_email.$campaign_id.$c_send_count.");
		return SRegex::add_a_href_tracker($body, $tracker_url);
	}

	private function set_open_tracker(string $body, Email $email, Campaign $campaign)
	{
		$encrypted_email = SCrypt::encrypt($email->sha256, $email->crypt_key);
		$campaign_id = $campaign->id;
		$c_send_count = $campaign->send_count;
		$tracker_url = URL::to("/open/$encrypted_email.$campaign_id.$c_send_count");
		return $body . PHP_EOL . '<img src=\'' . $tracker_url . '\' alt="">';
	}

	private function set_unsub_link(string $body, Email $email)
	{
		return str_replace(
			'{$EMAIL_DEZABO_LINK}',
			'<a href="' . $email->get_unsubscribe_link() .'">se désabonner</a>',
			$body
		);
	}
}