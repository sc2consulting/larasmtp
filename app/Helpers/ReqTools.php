<?php


namespace App\Helpers;


use Illuminate\Http\Request;

class ReqTools
{
	public static function notNullArgs(Request $req, $required)
	{
		foreach ($required as $str) {
			if ($req->get($str) == null) {
				return false;
			}
		}
		return true;
	}
}