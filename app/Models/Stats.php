<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

class Stats extends Model
{
    use HasFactory;

    public function campaign(): Relations\HasOne
    {
    	return $this->hasOne(Campaign::class);
    }
}
