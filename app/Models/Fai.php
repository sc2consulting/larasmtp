<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Fai extends Model
{
    use HasFactory;

    public function emails()
    {
    	return $this->hasMany(Email::class);
    }

    public static function get_fai_id(string $email) {
    	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    		return null;
    	$email_domain = explode('@', $email)[1];

    	foreach (Fai::whereGroupType(0)->get() as $fai) {
    		if ($email_domain == $fai->domain)
    			return $fai->id;
	    }
	    foreach (Fai::whereGroupType(1)->get() as $fai) {
		    if (str_ends_with($email_domain, $fai->domain))
			    return $fai->id;
	    }
	    return 0;
    }
}
