<?php

namespace App\Models;

use App\Helpers\SCrypt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class Email extends Model
{
    use HasFactory;

    public function __construct($email = null)
    {
	    parent::__construct();
	    if ($email != null) {
		    $this->email = $email;
		    $this->sha256 = \hash('sha256', $this->email);
		    $this->crypt_key = Str::random();
		    $this->fai_id = Fai::get_fai_id($email);
	    }
    }

    // Relations
    public function fai()
    {
    	return $this->belongsTo(Fai::class);
    }

    public function campaigns()
    {
    	return $this->belongsToMany(Campaign::class, 'campaign_email');
    }

	public function open_campaigns()
	{
		return $this->belongsToMany(Campaign::class, 'email_campaign_opens');
	}

	public function click_campaigns()
	{
		return $this->belongsToMany(Campaign::class, 'email_campaign_clicks');
	}

	// functions
	public static function store_file(File $file)
    {
		$fh = fopen($file->path, 'r');

		$id_list = [];
		while ($line = fgets($fh)) {
			$line = trim($line);
			if (!filter_var($line, FILTER_VALIDATE_EMAIL)) {
				return false;
			}
			if (!($email = Email::whereEmail($line)->first())) {
				$email = new Email($line);
				$email->save();
			}
			array_push($id_list, $email->id);
		}
		return $id_list;
    }

	public function get_unsubscribe_link(): string
	{
		$crypt_param = SCrypt::encrypt($this->sha256, $this->crypt_key);
		return URL::to('/unsubscribe/' . $crypt_param);
	}
}
