<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use mysql_xdevapi\Exception;

class File extends Model
{
    use HasFactory;

    public static function store($src_file, $folder)
    {
	    $filename = date("Y-m-d_H:i:s") . '_' . $src_file->getClientOriginalName();
	    $src_file->move(
		    storage_path(). $folder,
		    $filename
	    );

	    $file = new File();
	    $file->path = storage_path() . $folder . '/' . $filename;
        $file->size = 0;
	    $file->save();
	    return $file;
    }
}
