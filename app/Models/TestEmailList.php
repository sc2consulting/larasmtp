<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestEmailList extends Model
{
    use HasFactory;

    public function emails()
    {
    	return $this->belongsToMany(Email::class, 'test_list_email_relations');
    }

    public function get_emails()
    {
    	$list = [];
    	foreach ($this->emails as $email) {
    		$list []= $email;
	    }
    	return $list;
    }
}
