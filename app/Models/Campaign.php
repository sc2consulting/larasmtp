<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;

class Campaign extends Model
{
    use HasFactory;

    public function __construct(array $attributes = [])
    {
	    parent::__construct($attributes);
	    $stats = new Stats();
	    $stats->save();
	    $this->stats_id = $stats->id;
    }

	public function emails(): Relations\BelongsToMany
	{
		return $this->belongsToMany(Email::class, 'campaign_email');
	}

	public function stats(): Relations\BelongsTo
	{
		return $this->belongsTo(Stats::class);
	}

	public function open_emails(): Relations\BelongsToMany
	{
		return $this->belongsToMany(Email::class, 'email_campaign_opens');
	}

	public function click_emails(): Relations\BelongsToMany
	{
		return $this->belongsToMany(Email::class, 'email_campaign_clicks');
	}

	public function transporter(): Relations\BelongsTo
	{
		return $this->belongsTo(Transporter::class);
	}

	public function getSwiftTransporterObject(): \Swift_SmtpTransport
	{
		$transporter_info = $this->transporter;
		if ($transporter_info->method != 'SMTP')
			throw new \Exception('not an smtp transporter');
		$transporter = new \Swift_SmtpTransport(
			$transporter_info->host,
			$transporter_info->port
		);
		$transporter->setUsername($transporter_info->username);
		$transporter->setPassword($transporter_info->password);
		return $transporter;
	}

	/**
	 *  update how many times this campaign sent a batch
	 */
	public function update_send_count()
	{
		$this->send_count += 1;
		$this->save();
	}
}
