<?php

namespace App\Console\Commands;

use App\Helpers\Mailers\OvhRouter;
use Illuminate\Console\Command;

class update_MailHub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config:update_MailHub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	echo (OvhRouter::add_to_DB() ? "added ovh_router" : "ovh_router already exists");
        return 0;
    }
}
