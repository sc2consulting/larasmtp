<?php

namespace App\Console\Commands;

use App\Models\Email;
use App\Models\Fai;
use Illuminate\Console\Command;

class load_unsub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:load_unsub {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $fh = fopen($this->argument('path'), 'r');
	    $count = 0;
	    while ($email = fgets($fh)) {
	    	$email = trim($email);
	    	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			    if (!$email_obj = Email::whereEmail($email)->first()) {
			    	$email_obj = new Email($email);
			    }
			    $email_obj->unsubscribe = true;
			    $email_obj->save();
			    $count++;
		    }
	    	else
	    		echo $email . PHP_EOL;
	    }
        return 0;
    }
}
