<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::get('/', function () {
    return redirect('/dashboard');
});
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');


// email
Route::get('/unsubscribe/{data}', [Controllers\StatsController::class, 'unsubscribe']);
Route::get('/open/{data}', [Controllers\StatsController::class, 'open']);
Route::get('/click/{data}', [Controllers\StatsController::class, 'click']);

// campaign
Route::get('/campaign', [Controllers\CampaignController::class, 'getAllView'])->name('campaigns');
Route::get('/campaign/get/{id}', [Controllers\CampaignController::class, 'getOneView'])->name('campaign');
//Route::get('/campaign/add', [Controllers\CampaignController::class, 'addOneView']);
Route::post('/campaign/store', [Controllers\CampaignController::class, 'addOne']);
Route::post('/campaign/delete', [Controllers\CampaignController::class, 'deleteOne']);
Route::post('/campaign/edit', [Controllers\CampaignController::class, 'editOne']);

// campaign email list
Route::get('/campaign/get_emails/{id}', [Controllers\CampaignController::class, 'getEmailListView'])->name('campaign_emails');
Route::post('/campaign/add_emails', [Controllers\CampaignController::class, 'addEmailList']);
Route::post('/campaign/email/delete', [Controllers\CampaignController::class, 'removeEmail']);
Route::post('/campaign/email/delete_list', [Controllers\CampaignController::class, 'removeEmails']);

// campaign exec
Route::post('/campaign/exec', [Controllers\CampaignController::class, 'exec_campaign']);
Route::post('/campaign/exec_test', [Controllers\CampaignController::class, 'exec_test']);

// Transporter
Route::get('/transporters', [Controllers\TransporterController::class, 'getTransporterView']);

// test lists
Route::get('/test_lists', [Controllers\TestEmailListController::class, 'getView'])->name('Test Email Lists');
Route::post('/test_lists/add', [Controllers\TestEmailListController::class, 'add_list']);
Route::post('/test_lists/remove', [Controllers\TestEmailListController::class, 'remove_list']);