<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('sha256');
			$table->integer('fai_id');
			$table->boolean('unsubscribe')->default(false);
			$table->timestamp('last_sent')->nullable();

			// stats
	        $table->integer('deliver')->default(0);
	        $table->integer('fails')->default(0);
	        $table->integer('open')->default(0);
	        $table->integer('click')->default(0);
	        $table->integer('bounce')->default(0);
	        $table->integer('complaint')->default(0);

	        // crypt
	        $table->string('crypt_key');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
