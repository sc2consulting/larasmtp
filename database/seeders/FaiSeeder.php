<?php

namespace Database\Seeders;

use App\Models\Fai;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*
    	 * MAKE SURE TO HAVE GLOBAL DOWN ON THE LIST
    	 * DROP AND SEED THE TABLE ON CHANGES !
    	 */
        DB::table('fais')->insert([
        	['fai_group_id' => 1, 'name' => 'orange', 'domain' => 'orange.fr'],
			['fai_group_id' => 1, 'name' => 'orange', 'domain' => 'wanadoo.fr'],
			['fai_group_id' => 1, 'name' => 'orange', 'domain' => 'voila.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'free.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'freesbee.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'libertysurf.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'worldonline.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'online.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'alicepro.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'aliceadsl.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'alicemail.fr'],
			['fai_group_id' => 2, 'name' => 'free', 'domain' => 'infonie.fr'],
			['fai_group_id' => 3, 'name' => 'gmail', 'domain' => 'gmail.com'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'sfr.fr'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'neuf.fr'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'cegetel.net'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'club-internet.fr'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'numericable.fr'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'numericable.com'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'noos.fr'],
			['fai_group_id' => 4, 'name' => 'sfr', 'domain' => 'neufcegetel.fr'],
			['fai_group_id' => 5, 'name' => 'laposte', 'domain' => 'laposte.net'],
			['fai_group_id' => 5, 'name' => 'laposte', 'domain' => 'laposte.fr'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'hotmail.fr'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'msn.fr'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'live.fr'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'outlook.fr'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'outlook.com'],
			['fai_group_id' => 7, 'name' => 'hotmail', 'domain' => 'msn.com'],
			['fai_group_id' => 8, 'name' => 'yahoo', 'domain' => 'yahoo.fr'],
			['fai_group_id' => 8, 'name' => 'yahoo', 'domain' => 'yahoo.com'],
			['fai_group_id' => 9, 'name' => 'aol', 'domain' => 'aol.com'],
			['fai_group_id' => 9, 'name' => 'aol', 'domain' => 'aol.fr'],
			['fai_group_id' => 10, 'name' => 'bbox', 'domain' => 'bbox.fr'],
			['fai_group_id' => 11, 'name' => 'apple', 'domain' => 'icloud.com'],
			['fai_group_id' => 15, 'name' => 'tiscali', 'domain' => 'tiscali.fr'],
        ]);

	    // globals
	    DB::table('fais')->insert([
		    ['fai_group_id' => 6, 'name' => 'autre', 'domain' => '.com', 'group_type' => 1],
	        ['fai_group_id' => 12, 'name' => 'france', 'domain' => '.fr', 'group_type' => 1],
	        ['fai_group_id' => 13, 'name' => 'suisse', 'domain' => '.ch', 'group_type' => 1],
	        ['fai_group_id' => 14, 'name' => 'belgique', 'domain' => '.be', 'group_type' => 1],
	    ]);
    }
}

